/*************************************************************************
 * @File Name: image_stitch.c
 * @Description: 
 * @Author: mrs1023
 * @Mail: 1164801279@qq.cn
 * @Created Time: 2021.07.31
 * @Modification: 
 ************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <errno.h>


int splice_direct = 0;  // 0: vertical,  1: horizontal
int img_width = 640;
int img_height = 360;
FILE *fp_0 = NULL;
FILE *fp_1 = NULL;

/******************************************************************************
 * Function: yuv_vertical_splice
 * Description: vertical splice 2 yuv buf
 * parameter:	buf_up		top image buf
 *				buf_down	bottom image buf
 *				buf_out		output image buf
 *				width		image width
 *				height		image height
 * Return: On success, returns 0; on error, it returns -1 
 *****************************************************************************/

int yuv_vertical_splice(unsigned char* buf_up, unsigned char* buf_down, 
					unsigned char* buf_out, int width, int height)
{	
	if (!buf_up || !buf_down || !buf_out) {
		printf("invalid parameter.\n");
		return -1;
	}
	
	unsigned int size = width * height;

	memcpy(buf_out, buf_up, size);
	memcpy(buf_out + size, buf_down, size);
	memcpy(buf_out + size * 2, buf_up + size, size / 2);
	memcpy(buf_out + size * 5 / 2, buf_down + size, size / 2);

	return 0;
}

/******************************************************************************
 * Function: yuv_horizontal_splice
 * Description: Horizontal splice 2 yuv buf
 * parameter:	buf_left	left image buf
 *				buf_right	right image buf
 *				buf_out		output image buf
 *				width		image width
 *				height		image height
 * Return: On success, returns 0; on error, it returns -1 
 *****************************************************************************/

int yuv_horizontal_splice(unsigned char* buf_left, unsigned char* buf_right, 
					unsigned char* buf_out, int width, int height)
{	
	if (!buf_left || !buf_right || !buf_out) {
		printf("invalid parameter.\n");
		return -1;
	}
	
	int i;
	unsigned char *y0 = buf_left;
	unsigned char *y1 = buf_right;
	unsigned char *uv0 = y0 + width * height;
	unsigned char *uv1 = y1 + width * height;

	// copy Y
	for (i = 0; i < height; i++)
	{
		memcpy(buf_out, y0, width);
		buf_out += width;
		y0 += width;

		memcpy(buf_out, y1, width);
		buf_out += width;
		y1 += width;
	}

	// copy UV
	for (i = 0; i < height / 2; i++)
	{
		memcpy(buf_out, uv0, width);
		buf_out += width;
		uv0 += width;
		
		memcpy(buf_out, uv1, width);
		buf_out += width;
		uv1 += width;
	}

	return 0;
}


int yuv_splice(void)
{
	// output file
	FILE *fp = NULL;
	char file_name[128];
	if (splice_direct == 0) // vertical
		sprintf(file_name, "output_%dx%d.yuv", img_width, img_height * 2);
	else
		sprintf(file_name, "output_%dx%d.yuv", img_width * 2, img_height);
	fp = fopen(file_name, "w");
	if (fp == NULL) {
		printf("open %s err: %s\n", file_name, strerror(errno));
		goto ERR_1;
	}
	printf("OUTPUT:%s\n", file_name);
	
	// input buffer
	unsigned int size = img_width * img_height * 3 / 2;
	unsigned char *buf_0 = (unsigned char*)malloc(size);
	if (buf_0 == NULL) {
		printf("malloc failed:%s\n", strerror(errno));
		goto ERR_1;
	}
	unsigned char *buf_1 = (unsigned char*)malloc(size);
	if (buf_1 == NULL) {
		printf("malloc failed:%s\n", strerror(errno));
		goto ERR_0;
	}
	memset(buf_0, 0, size);
	memset(buf_1, 0, size);

	// output buffer
	unsigned int out_size = img_width * img_height * 3;
	unsigned char *buf = (unsigned char*)malloc(out_size);
	if (buf == NULL) {
		printf("malloc failed:%s\n", strerror(errno));
		goto ERR_0;
	}
	

	while ((!feof(fp_0)) && (!feof(fp_1))) {
		if (size != fread(buf_0, 1, size, fp_0)) {
			if (ferror(fp_0))
				printf("fread err: %s\n", strerror(errno));
			else
				printf("process complete.\n");
			goto ERR_0;
		}
		if (size != fread(buf_1, 1, size, fp_1)) {
			if (ferror(fp_1))
				printf("fread err: %s\n", strerror(errno));
			else
				printf("process complete.\n");
			goto ERR_0;
		}
	
		if (splice_direct == 0)
			yuv_vertical_splice(buf_0, buf_1, buf, img_width, img_height);
		else
			yuv_horizontal_splice(buf_0, buf_1, buf, img_width, img_height);
		fwrite(buf, 1, out_size, fp);
	}


ERR_0:
	if (buf_0)
		free(buf_0);
	if (buf_1)
		free(buf_1);
ERR_1:
	fclose(fp);

	return 0;
}


void usage_exit(char *arg)
{
	printf( "\nusage:%s [-vh] [-i <file> <file>] [-s <width> <height>]\n\n"
			"  -v     Vertical splicing\n"
			"  -h     Horizontal splicing\n"
			"  -i     2 input file, yuv420sp\n"
			"  -s     image size, width height\n\n",arg);
	printf("eg:\n"
			"  %s -v -i input_0.yuv input_1.yuv -s 1920 1080\n"
			"  %s -h -i input_0.yuv input_1.yuv -s 1280 720\n\n", arg, arg);
	exit(1);
}

int parse_option(int argc, char **argv)
{
	int ch = -1;

	while ((ch = getopt(argc, argv, "vhi:s:")) != -1) {
		switch (ch) {
		case 'v':
			splice_direct = 0;
			break;
		case 'h':
			splice_direct = 1;
			break;
		case 'i':
			fp_0 = fopen(optarg, "r");
			if (fp_0 == NULL) {
				printf("[%d]open %s failed:%s\n", __LINE__, optarg, strerror(errno));
				return -1;
			}
			printf("input file:%s\n", optarg);
			fp_1 = fopen(argv[optind], "r");
			if (fp_1 == NULL) {
				printf("[%d]open %s failed:%s\n", __LINE__, argv[optind], strerror(errno));
				fclose(fp_0);
				return -1;
			}
			printf("input file:%s\n", argv[optind]);
			break;
		case 's':
			img_width = atoi(optarg);
			img_height = atoi(argv[optind]);
			
			printf("image size: %d x %d\n", img_width, img_height);
			break;
		default:
			usage_exit(argv[0]);
			printf("invalid parameter.\n");
		}
	}
	return 0;
}

int main(int argc, char *argv[ ])
{
	printf("\033[1;32m%s %s\033[0m\n", __DATE__, __TIME__);
	printf("\033[1;32mYUV SPLICING DEMO\033[0m\n\n");

	int ret;
	ret = parse_option(argc, argv);
	if (ret || fp_0 == NULL || fp_1 == NULL) {
		usage_exit(argv[0]);
	}

	printf("splicing direction:%s\n", splice_direct == 0 ? "vertical" : "horizontal");
	
	yuv_splice();

	return 0;
}
